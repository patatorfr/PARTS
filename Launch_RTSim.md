## Download on Ubuntu 22.04

In order to use RTSim, you have to import this repository to your local directory :
   - Make sure you can clone repository from GitLab : `sudo apt-get update && sudo apt install git`
   - Create a PARTS repository on your computer     : `mkdir PARTS`
   - Move to this directory                         : `cd PARTS`
   - Clone the RTSim framework in the directory     : `git clone https://gitlab.com/patatorfr/PARTSim.git` . You can also clone the Partprof framework if you want to use it : `git clone https://gitlab.com/patatorfr/partprof.git`

## Compile RTSim to create the executable

Once you have downloaded all the needed files on your computer, you can use the CMakefiles to create the RTSim executable :
   - Move to PARTSim directory : `cd PARTSim`
   - Make sure you have g++ and cmake packages on your computer : `sudo apt-get update && sudo apt-get install g++ && sudo apt-get install cmake`
   - Install files inside the subdirectories : ` git submodule update --recursive --init`
   - Build the executable : `./tools/builder.py -cJ build`
   
You now have the executable at `build/rtsim/rtsim`

## Launch a simulation

To launch a simulation, you have 2 possibilities :
   1) `./build/rtsim/rtsim  simconf/systems/odroid-xu3-partitioned-tb.yml simconf/tasksets/odroid-xu3-dag.yml 10000 --trace trace-out.txt` \
   This line will launch a simulation runing the taskset with a DAG inside, on the system definition for an ODROID-XU3 board, 
   during a time of 10.000 time units. \
   Finally we can access the output data int the 'trace-out.txt' file.

   2) `sh CVS_output.sh odroid-xu3-partitioned-tb.yml odroid-xu3-dag.yml 2000 test.txt` \
   You can access various informations about the simulation (CPU architecture / taskset / tasks finish times) in the file called
   'rtsim_convert.cvs'. \
   To better understand the process of the 'CVS_output.sh' file, you can remove the last 3 lines of the code, and look at the 
   datas inside the file that will not be removed.

 
