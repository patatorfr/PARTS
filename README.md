# PARTS - Power-Aware RT Profiling and Simulation

This is a project that aims to simulate better real-time systems running on specific embedded platforms.

**For more info see the companion paper**: https://www.gabrieleara.it/publications/conferences/2022-sac

----------------------------------------------------------

### Rémi's informations update :

This project is an extension of the framework RTSim, which makes it possible to simulate the behavior of a processor receiving a bunch of tasks, but also collect datas about it's powerconsumption through the simulation. PARTS framework is divided in 2 different libraries :

     1) Partsim :
            Original RTSim framework. With this library, you can create various processor (CPU) architectures (number of CPU islands / 
        number of cores on each island / scheduling policy), the taskset running  on the CPU's, and if different policies such as 
            DVFS are used or not during the simulation.
        The RTSim executable takes those parameters as input, and returns a file with informations about the scheduling/ending time
        of the tasks from the taskset.
            To create and run an RTSim simulation, follow the instruction from [Launch_RTSim] file.
            If you want further informations about the framework, please refer to the Readme in the corresponding subdirectory, and to
            Gabriele's paper.

     2) Partprof :
            This library is the one added to the old RTsim framework. It contains an executable that you could be running on a separate
        physical CPU to collect informations in various scenarios. Then those datas are processed and stored in a table format, used in 
        Partsim to recreate the energy consumption model of the tested CPU. This library is the one that help to create the model of the 
        processor in term of powerconsumption.
            You can find more informations about the way it's achieved and how to use this library in the corresponding subdirectory.


